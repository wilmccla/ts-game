﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPos : MonoBehaviour
{

    private GameMaster gm;
    public GameObject player;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        player = GameObject.FindGameObjectWithTag("Player");
        transform.position = gm.lastCheckPointPos;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            player.transform.position = gm.lastCheckPointPos;
        }
    }
}
